# create azure function
- https://docs.microsoft.com/en-us/azure/azure-functions/create-first-function-cli-java?tabs=bash%2Cazure-cli%2Cbrowser
- `mvn --version` - 3.8.4
- `func --version` - 3.0.3904
```
mvn clean package
mvn azure-functions:run
```

# deploy func app
```
mvn clean package azure-functions:deploy
```
- check
```
curl https://alaczynski-fa2.azurewebsites.net/api/product-get?productId=123
```

# create logic app
